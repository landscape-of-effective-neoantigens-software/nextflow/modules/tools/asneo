#!/usr/bin/env nextflow

process bwa_mem {

  tag "${dataset}/${pat_name}/${run}"
  label 'asneo_container'
  label 'asneo_mem'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(junctions), path(hla_alleles)
  path(fa)
  val parstr

//  output:
//  tuple val(pat_name), val(run), val(dataset), path('*.sam'), emit: sams

  script:
  """
  # Parse alleles here
 
  python /ASNEO/ASNEO.py \
  -j ${junctions} \
  -g ${fa} \
  -l 8,9,10,11 \
  -o ${dataset}-${pat_name}-${run}.asneo_results \
  -p ${task.cpus} \
  ${parstr}
  """
}
